/**
 * Created by sissoko on 25/02/2017.
 */
// Angular 2
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';

import 'rxjs';

// Angular Redux
import '@angular-redux/store';
import 'redux';
import "redux-logger"
