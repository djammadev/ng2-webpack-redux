import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {Credentials} from "../app.model";
import {AppState} from "../app.state";
import {NgRedux} from "@angular-redux/store";
import {LoginActions, LOGIN_ACTIONS} from "./login.action";

@Component({
    selector: 'login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    credentials: Credentials;
    message: string;

    constructor(private ngRedux: NgRedux<AppState>, private actions: LoginActions, private router: Router) {
        this.credentials = <Credentials>{};
    }

    logIn() {
        let action = this.actions.logIn(this.credentials);
        action.payload.then(user => {
            this.ngRedux.dispatch({
                type: LOGIN_ACTIONS.SET_CONNECTED,
                payload: {connected: user, auth: true}
            });
            this.router.navigate(['/'])
        }, error => {
            this.ngRedux.dispatch({
                type: LOGIN_ACTIONS.SET_CONNECTED,
                payload: {connected: {}, auth: false}
            });
            let messageJson = error.json();
            this.message = messageJson.message;
        });
    }

}
