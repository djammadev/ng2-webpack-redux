import {Injectable} from "@angular/core";
import {LoginService} from "./login.service";
import {Credentials, User} from "../app.model";
import {AppAction} from "../common/app.action";

export const LOGIN_ACTIONS = {
    SET_CONNECTED: "SET_CONNECTED",
    SET_AUTH: "SET_AUTH",
    LOGIN_IN_PROGRESS: "LOGIN_IN_PROGRESS",
    FETCHING_USER: "FETCHING_USER",
    LOGGING_OUT: "LOGGING_OUT",
    REGISTRATION_IN_PROGRESS: "REGISTRATION_IN_PROGRESS",
};

@Injectable()
export class LoginActions {

    constructor(private service: LoginService) {
    }

    logIn(credentials: Credentials): AppAction {
        return {
            type: LOGIN_ACTIONS.LOGIN_IN_PROGRESS,
            payload: this.service.logIn(credentials)
        }
    }

    fetchUser(): AppAction {
        return {
            type: LOGIN_ACTIONS.FETCHING_USER,
            payload: this.service.fetchUser()
        }
    }

    logOut(): AppAction {
        return {
            type: LOGIN_ACTIONS.LOGGING_OUT,
            payload: this.service.logOut()
        }
    }

    register(user: User): AppAction {
        return {
            type: LOGIN_ACTIONS.REGISTRATION_IN_PROGRESS,
            payload: this.service.register(user)
        }
    }
}