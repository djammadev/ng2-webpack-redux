import {Injectable} from '@angular/core';
import {Http, Headers, Response} from "@angular/http";
import {Credentials, User} from "../app.model";

@Injectable()
export class LoginService {
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) {
    }

    logIn(credentials: Credentials): Promise<User> {
        return this.http
            .post(this.getAuthUrl(), JSON.stringify(credentials), {headers: this.headers})
            .toPromise()
            .then(this.handleUser.bind(this))
            .catch(this.handleError.bind(this));
    }

    fetchUser(): Promise<User> {
        return this.http.get(this.getAuthUrl())
            .toPromise()
            .then(this.handleUser.bind(this))
            .catch(this.handleError.bind(this));
    }

    logOut(): Promise<any> {
        return this.http
            .delete(this.getAuthUrl())
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    register(user: User): Promise<User> {
        return this.http
            .post(this.getRegisterUrl(), JSON.stringify(user), {headers: this.headers})
            .toPromise()
            .then(this.handleUser.bind(this))
            .catch(this.handleError.bind(this));
    }

    private handleUser(res: Response): User {
        console.log("Response...", res);
        let resJson = res.json();
        let data = resJson.data ? resJson.data[0] : resJson;
        return <User>data;
    }

    private getAuthUrl(): string {
        return "/api/auth"
    }

    private getRegisterUrl(): string {
        return `${this.getAuthUrl()}/register`
    }

    private handleError(error: any): Promise<any> {
        console.log("Error", error);
        return Promise.reject(error);
    }
}
