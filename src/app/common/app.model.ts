/**
 * Created by sissoko on 25/02/2017.
 */

export abstract class ModelBase {
    id?: number;
}

export class MessageError {
    code: number;
    message: string;
}

export class Pagination<T> {
    page: number;
    pageSize: number;
    totalEntityCount: number;
    pageCount: number;
    entities: T[];
}