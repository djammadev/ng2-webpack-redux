import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
/**
 * Created by sissoko on 03/03/2017.
 */

@Injectable()
export class UtilService {

    constructor(private router: Router) {
    }

    inOrOut(val: number): string {
        if (val >= 0)
            return "in";
        return "out";
    }

    handleError(error: any) {
        console.log("Error...", error);
        if (error.code == 401) {
            this.router.navigate(['/login']);
        }
    }
}