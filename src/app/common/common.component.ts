import {Input, EventEmitter, Output} from "@angular/core";
import {ModelBase} from "./app.model";
import {NgRedux} from "@angular-redux/store";
import {IAppState} from "./app.state";
import {BaseActions} from "./app.action";
import {UtilService} from "./app.utils";

export abstract class BaseList<T extends ModelBase> {
    @Input() values: T[] = [];
    @Output() onRemove: EventEmitter<T> = new EventEmitter();

    remove(value: T) {
        this.onRemove.emit(value);
    }
}

export abstract class BaseNewComponent<T extends ModelBase> {
    @Input() details: boolean = true;
    @Input() value: T;
    message: string;
    values: T[] = [];
    ngRedux: NgRedux<IAppState>;
    actions: BaseActions<T>;
    utils: UtilService;

    @Output() onHide: EventEmitter<any> = new EventEmitter();

    constructor(ngRedux: NgRedux<IAppState>,
                actions: BaseActions<T>,
                utils: UtilService) {
        this.ngRedux = ngRedux;
        this.actions = actions;
        this.utils = utils;
    }

    add() {
        let action;
        if (this.value.id == null)
            action = this.actions.create(this.value);
        else
            action = this.actions.update(this.value);
        action.payload.then(res => {
            //noinspection TypeScriptValidateTypes
            this.values = [res, ...this.values];
            this.ngRedux.dispatch({
                type: action.type,
                payload: res
            });
            this.clear();
        }, this.handleError.bind(this))
    }

    remove(value: T) {
        this.values = this.values.filter(t => t.id != value.id);
        let action = this.actions.remove(value);
        action.payload.then(res => {
            this.ngRedux.dispatch({
                type: action.type,
                payload: value
            });
        }, this.handleError.bind(this));
    }

    hide(event: any) {
        this.onHide.emit(event);
        return false;
    }

    private clear() {
        this.value = <T>{};
        this.message = null;
    }

    inOrOut(val: number): string {
        return this.utils.inOrOut(val);
    }

    private handleError(error: any) {
        this.message = error.message;
        this.utils.handleError(error);
    }
}