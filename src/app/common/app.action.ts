import {Action} from "redux";
import {Injectable} from "@angular/core";
import {BaseService} from "./app.service";
import {ModelBase} from "./app.model";

@Injectable()
export abstract class BaseActions<T extends ModelBase> {
    service: BaseService<T>;

    constructor(service: BaseService<T>) {
        this.service = service;
    }

    abstract getActionKeys();

    /**
     *
     * @param pageNumber
     * @param pageSize
     * @returns {{type: string, payload: Promise<Pagination<T>>}}
     */
    getPage(pageNumber: number = 1, pageSize: number = 100): AppAction {
        return {
            type: this.getAction("GET_PAGE"),
            payload: this.service.getPage(pageNumber, pageSize)
        }
    }

    /**
     *
     * @param id
     * @returns {{type: string, payload: Promise<T>}}
     */
    get(id: number): AppAction {
        return {
            type: this.getAction("GET_BY_ID"),
            payload: this.service.get(id)
        }
    }

    /**
     *
     * @param value
     * @returns {{type: string, payload: Promise<T>}}
     */
    create(value: T): AppAction {
        return {
            type: this.getAction("CREATE"),
            payload: this.service.create(value)
        }
    }

    /**
     *
     * @param value
     * @returns {{type: string, payload: Promise<T>}}
     */
    update(value: T): AppAction {
        return {
            type: this.getAction("UPDATE"),
            payload: this.service.update(value)
        }
    }

    /**
     *
     * @param value
     * @returns {{type: string, payload: Promise<T>}}
     */
    remove(value: T): AppAction {
        return {
            type: this.getAction("DELETE"),
            payload: this.service.remove(value)
        }
    }

    /**
     *
     * @param key
     * @returns {any}
     */
    getAction(key: string): string {
        let actionKeys = this.getActionKeys();
        return actionKeys[key];
    }
}

export interface AppAction extends Action {
    payload?: any
}


