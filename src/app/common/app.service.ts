import {ModelBase, Pagination} from "./app.model";
import {Headers, Http} from "@angular/http";

export abstract class BaseService<T extends ModelBase> {
    protected headers = new Headers({'Content-Type': 'application/json'});

    http: Http;

    constructor(http: Http) {
        this.http = http;
    }

    abstract getUri(): string;

    getUrl(): string {
        return `/api${this.getUri()}`;
    }

    getPage(pageNumber: number = 1, pageSize: number = 100): Promise<Pagination<T>> {
        let url = `${this.getUrl()}?page=${pageNumber}&perPage=${pageSize}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError.bind(this));
    }

    get(id: number): Promise<T> {
        const url = `${this.getUrl()}/${id}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError.bind(this));
    }

    create(value: T): Promise<T> {
        return this.http
            .post(this.getUrl(), JSON.stringify(value), {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    update(value: T): Promise<T> {
        const url = `${this.getUrl()}/${value.id}`;
        return this.http
            .put(url, JSON.stringify(value), {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError.bind(this));
    }

    remove(value: T): Promise<T> {
        const url = `${this.getUrl()}/${value.id}`;
        return this.http.delete(url)
            .toPromise()
            .then(() => value)
            .catch(this.handleError.bind(this));
    }

    protected handleError(error: any): Promise<any> {
        return Promise.reject(error.json() || error);
    }
}