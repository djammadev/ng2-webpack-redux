import {ModelBase, Pagination} from "./app.model";

export interface IAppState {
}

export interface BaseState<T extends ModelBase> {
    values?: Pagination<T>;
    value?: T;
}