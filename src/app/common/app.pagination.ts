import {AppState} from "../app.state";
import {NgRedux} from "@angular-redux/store";
import {BaseActions} from "./app.action";
import {Input} from "@angular/core";
/**
 * Created by sissoko on 27/02/2017.
 */


export abstract class PaginationComponent<T> {
    @Input() prevText: string = "Previous";
    @Input() nextText: string = "Next";
    page: number = 1;
    pageSize: number = 10;
    pageCount: number = 0;
    totalEntityCount: number = 0;

    ngRedux: NgRedux<AppState>;
    actions: BaseActions<T>;

    constructor(ngRedux: NgRedux<AppState>, actions: BaseActions<T>) {
        this.ngRedux = ngRedux;
        this.actions = actions;
    }

    hasNext(): boolean {
        return this.page < this.pageCount
    }

    hasPrev(): boolean {
        return this.page > 1;
    }

    next(event) {
        if (this.hasNext()) {
            this.load(this.page + 1, this.pageSize);
        }
    }

    previous(event) {
        if (this.hasPrev()) {
            this.load(this.page - 1, this.pageSize);
        }
    }

    protected load(page: number, pageSize: number) {
        let action = this.actions.getPage(page, pageSize);
        action.payload.then(page => {
            this.ngRedux.dispatch({
                type: this.actions.getAction('SET_VALUES'),
                payload: page
            });
        });
    }
}