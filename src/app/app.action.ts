import {Injectable} from "@angular/core";
import {BaseActions, AppAction} from "./common/app.action";
import {Transaction, Budget} from "./app.model";
import {TransactionService, BudgetService} from "./app.service";

export const TRANSACTION_ACTIONS = {
    GET_PAGE: "GET_TRANSACTION_PAGE",
    GET_BY_ID: "GET_TRANSACTION_BY_ID",
    CREATE: "CREATE_TRANSACTION",
    UPDATE: "UPDATE_TRANSACTION",
    DELETE: "DELETE_TRANSACTION",
    SET_VALUES: "SET_TRANSACTION_VALUES",
    SET_VALUE: "SET_TRANSACTION_VALUE",
    GET_AMOUNTS: "GET_AMOUNTS",
    SET_BUDGET: "SET_BUDGET"
};

export const BUDGET_ACTIONS = {
    GET_PAGE: "GET_BUDGET_PAGE",
    GET_BY_ID: "GET_BUDGET_BY_ID",
    CREATE: "CREATE_BUDGET",
    UPDATE: "UPDATE_BUDGET",
    DELETE: "DELETE_BUDGET",
    SET_VALUES: "SET_BUDGET_VALUES",
    SET_VALUE: "SET_BUDGET_VALUE",
    ADD_BUDGET: "ADD_BUDGET",
    GET_TRANSACTIONS: "GET_BUDGET_TRANSACTIONS",
    SET_AMOUNTS: "SET_AMOUNTS",
    DELETE_TRANSACTION: "DELETE_TRANSACTION",
    CREATE_TRANSACTION: "CREATE_TRANSACTION",
};

@Injectable()
export class TransactionActions extends BaseActions<Transaction> {

    service: TransactionService;

    constructor(service: TransactionService, private budgetService: BudgetService) {
        super(service);
        this.service = service;
    }

    getActionKeys() {
        return TRANSACTION_ACTIONS
    }

    /**
     *
     * @returns {{type: string, payload: Promise<any>}}
     */
    getAmounts(budgetId: number): AppAction {
        return {
            type: this.getAction("GET_AMOUNTS"),
            payload: this.service.getAmounts(budgetId)
        }
    }

    getTransactions(budgetId: number, page: number, pageSize: number): AppAction {
        return {
            type: BUDGET_ACTIONS.GET_TRANSACTIONS,
            payload: this.budgetService.getTransactions(budgetId, page, pageSize)
        }
    }

}

@Injectable()
export class BudgetActions extends BaseActions<Budget> {

    service: BudgetService;

    constructor(service: BudgetService) {
        super(service);
        this.service = service;
    }

    getActionKeys() {
        return BUDGET_ACTIONS
    }

    getTransactions(budgetId: number, page: number, pageSize: number): AppAction {
        return {
            type: BUDGET_ACTIONS.GET_TRANSACTIONS,
            payload: this.service.getTransactions(budgetId, page, pageSize)
        }
    }

    getAmounts(budgetId: number): AppAction {
        return {
            type: BUDGET_ACTIONS.SET_AMOUNTS,
            payload: this.service.getAmounts(budgetId)
        }
    }
}
