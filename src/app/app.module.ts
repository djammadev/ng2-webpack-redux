import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {RoutingModule} from "./app.routing";
import {NgReduxModule} from "@angular-redux/store";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {LoginService} from "./login/login.service";
import {LoginActions} from "./login/login.action";
import {RegisterComponent} from "./register/register.component";
import {LoginComponent} from "./login/login.component";
import {TransactionService} from "./app.service";
import {TransactionActions} from "./app.action";
import {BudgetModule} from "./budget/budget.module";
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RoutingModule,
        NgReduxModule,
        BudgetModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        DashboardComponent
    ],
    providers: [
        LoginService,
        LoginActions,
        TransactionService,
        TransactionActions
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
