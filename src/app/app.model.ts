import {ModelBase} from "./common/app.model";
/**
 * Created by sissoko on 25/02/2017.
 */
export class Credentials {
    username?: string;
    password?: string;
}

export class User extends ModelBase {
    firstName?: string;
    lastName?: string;
    username?: string;
    password?: string;
    admin?: boolean;
    lastConnection?: string;
}

export class Amounts {
    spent?: number;
    credit?: number;
    total?: number;
}

export class Transaction extends ModelBase {
    budgetId?: number;
    date?: string;
    amount: number;
    description: string;
}

export class Budget extends ModelBase {
    title: string;
    createDate?: string;
    description: string;
    amounts?: Amounts;
}