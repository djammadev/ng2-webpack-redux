/**
 * Created by sissoko on 22/02/2017.
 */
import {Component, OnInit} from "@angular/core";
import {NgRedux, select} from "@angular-redux/store";
import {AppState} from "./app.state";
import {Observable} from "rxjs";
import {User} from "./app.model";
import {rootReducer} from "./app.reducer";
import * as createLogger from "redux-logger";
import {LoginActions, LOGIN_ACTIONS} from "./login/login.action";
import {Router} from "@angular/router";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    @select((state: AppState) => state.login.connected) connected$: Observable<User>;
    connected: User;

    constructor(private ngRedux: NgRedux<AppState>,
                private loginActions: LoginActions,
                private router: Router) {
        this.ngRedux.configureStore(rootReducer, {}, [createLogger()]);
        this.connected$.subscribe(user => this.connected = user);
        this.fetchUser();
    }

    ngOnInit(): void {
    }

    fetchUser() {
        let action = this.loginActions.fetchUser();
        action.payload.then(user => {
            this.ngRedux.dispatch({
                type: LOGIN_ACTIONS.SET_CONNECTED,
                payload: {connected: user, auth: true}
            })
        }).catch(error => {
            if (error.code == 401) {
                this.router.navigate(['/login'])
            }
        });
    }

    logOut() {
        let action = this.loginActions.logOut();
        action.payload.then(res => {
            this.ngRedux.dispatch({
                type: LOGIN_ACTIONS.SET_CONNECTED,
                payload: {connected: {}, auth: false}
            })
        })
    }
}