import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {BaseService} from "./common/app.service";
import {Transaction, Amounts, Budget} from "./app.model";
import {Pagination} from "./common/app.model";

@Injectable()
export class TransactionService extends BaseService<Transaction> {

    constructor(http: Http) {
        super(http);
    }

    getUri(): string {
        return "/transaction";
    }

    getAmounts(budgetId: number): Promise<Amounts> {
        const url = `${this.getUrl()}/amounts?budgetId=${budgetId}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError.bind(this));
    }

}

@Injectable()
export class BudgetService extends BaseService<Budget> {

    constructor(http: Http) {
        super(http);
    }

    getUri(): string {
        return "/budget";
    }

    getTransactions(budgetId: number, page: number, pageSize: number): Promise<Pagination<Transaction>> {
        const url = `${this.getUrl()}/${budgetId}/transactions?page=${page}&perPage=${pageSize}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError.bind(this));
    }

    getAmounts(budgetId: number): Promise<Amounts> {
        const url = `${this.getUrl()}/${budgetId}/amounts`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError.bind(this));
    }
}