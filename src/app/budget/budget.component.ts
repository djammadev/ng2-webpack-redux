import {Component} from "@angular/core";
import {Observable} from "rxjs";
import {Budget, Amounts} from "../app.model";
import {Pagination} from "../common/app.model";
import {AppState} from "../app.state";
import {select, NgRedux} from "@angular-redux/store";
import {BudgetActions, BUDGET_ACTIONS} from "../app.action";
import {UtilService} from "../common/app.utils";
import {BaseList, BaseNewComponent} from "../common/common.component";

@Component({
    selector: 'budget',
    template: `
        <div class="budgets">
            <button *ngIf="!addNew" class="add add-budget btn btn-default" (click)="addBudget()"><i
                    class="glyphicon glyphicon-plus"></i></button>
            <new-budget *ngIf="addNew" [details]="false" (onHide)="addNew=false"></new-budget>
            <budget-list [values]="values" (onRemove)="remove($event)"></budget-list>
        </div>
    `
})
export class BudgetComponent {
    @select((state: AppState) => state.budgets.values) values$: Observable<Pagination<Budget>>;
    values: Budget[] = [];
    page: number = 1;
    pageSize: number = 6;
    addNew: boolean = false;

    constructor(private ngRedux: NgRedux<AppState>,
                private actions: BudgetActions,
                private utils: UtilService) {
        this.values$.subscribe(values => {
            if (values) {
                this.values = values.entities;
            }
        });
    }

    ngOnInit() {
        this.loadBudgets();
    }

    addBudget() {
        this.addNew = true;
    }

    remove(value: Budget) {
        let action = this.actions.remove(value);
        action.payload.then(res => {
            this.ngRedux.dispatch({
                type: BUDGET_ACTIONS.DELETE,
                payload: value
            });
        }).catch(this.handleError.bind(this));
    }

    inOrOut(val: number): string {
        return this.utils.inOrOut(val);
    }

    private loadBudgets() {
        let action = this.actions.getPage(this.page, this.pageSize);
        action.payload.then(page => {
                this.ngRedux.dispatch({
                    type: BUDGET_ACTIONS.SET_VALUES,
                    payload: page
                });
            },
            this.handleError.bind(this));
    }

    private handleError(error: any) {
        this.utils.handleError(error);
    }
}

@Component({
    selector: 'amounts',
    template: `
    <div class="row amounts">
        <div class="col-md-4 col-sm-4 col-xs-4 total-credit total-balance-{{inOrOut(amounts.credit)}}">
            <h4 class="balance-md balance-sm balance-xs"><span class="{{inOrOut(amounts.credit)}}">{{(amounts.credit || 0) | currency:'EUR':true:'1.2-2'}}</span>
            </h4>
            <p class="description">Total Credit</p>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 total-balance total-balance-{{inOrOut(amounts.total)}}">
            <h4 class="balance-md balance-sm balance-xs"><span class="{{inOrOut(amounts.total)}}">{{(amounts.total || 0) | currency:'EUR':true:'1.2-2'}}</span>
            </h4>
            <p class="description">Total Balance</p>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 total-spent total-balance-{{inOrOut(amounts.spent)}}">
            <h4 class="balance-md balance-sm balance-xs"><span class="{{inOrOut(amounts.spent)}}">{{(amounts.spent || 0) | currency:'EUR':true:'1.2-2'}}</span>
            </h4>
            <p class="description">Total Spent</p>
        </div>
    </div>
    `
})
export class AmountsComponent {
    @select((state: AppState) => state.budgets.amounts) amounts$: Observable<Amounts>;
    amounts: Amounts = <Amounts>{};

    /**
     *
     * @param ngRedux
     * @param utils
     */
    constructor(private ngRedux: NgRedux<AppState>, private utils: UtilService) {
        this.amounts$.subscribe(amounts => {
            if (amounts)
                this.amounts = amounts;
        });
    }

    inOrOut(val: number): string {
        return this.utils.inOrOut(val);
    }
}

@Component({
    selector: 'budget-list',
    template: `
        <div class="list-group">
            <span *ngFor="let budget of values"
               class="list-group-item transaction-{{inOrOut(budget.amounts.total)}}">
                <span class="badge {{inOrOut(budget.amounts.total)}} balance transaction-{{inOrOut(budget.amounts.total)}}-badge">{{budget.amounts.total | currency:'EUR':true:'1.2-2'}}</span>
                <h4 [routerLink]="['/budget', budget.id]" class="list-group-item-heading text-uppercase title">{{budget.createDate | date:'dd MMM.'}} {{budget.title}}</h4>
                <p class="list-group-item-text description text-uppercase">
                    {{budget.description ? budget.description : 'Description'}}
                    <span class="badge btn-danger delete pull-right" (click)="remove(budget)">X</span>
                </p>

            </span>
        </div>
    `
})
export class BudgetList extends BaseList<Budget> {

    constructor(private utils: UtilService) {
        super();
    }

    inOrOut(val: number): string {
        return this.utils.inOrOut(val);
    }
}

@Component({
    selector: 'new-budget',
    template: `
        <form role="form">
            <fieldset>
                <div class="form-group">
                    <input placeholder="Title" class="form-control" type="text" [(ngModel)]="value.title" name="title"/>
                </div>
                <div class="form-group">
                    <input placeholder="Description" class="form-control" type="text" [(ngModel)]="value.description"
                           name="description"/>
                </div>
                <div *ngIf="message">
                    <span class="alert-danger">{{message}}</span>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <button type="submit" (click)="add()" class="btn btn-lg btn-success btn-block">Add</button>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <button (click)="hide($event)" class="btn btn-lg btn-default btn-block">Hide</button>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr class="colorgraph">
        <div *ngIf="details" class="budgets new">
            <budget-list [values]="values" (onRemove)="remove($event)"></budget-list>
        </div>
    `
})
export class NewBudgetComponent extends BaseNewComponent<Budget> {

    constructor(ngRedux: NgRedux<AppState>,
                actions: BudgetActions,
                utils: UtilService) {
        super(ngRedux, actions, utils);
        this.value = <Budget>{};
    }

}