import {NgModule} from "@angular/core";
import {BudgetComponent, BudgetList, NewBudgetComponent, AmountsComponent} from "./budget.component";
import {BudgetService} from "../app.service";
import {BudgetActions} from "../app.action";
import {
    TransactionComponent, NewTransactionComponent, TransactionList,
    TransactionPaginationComponent
} from "../transaction/transaction.component";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {RoutingModule} from "../app.routing";
import {HttpModule} from "@angular/http";
import {UtilService} from "../common/app.utils";
/**
 * Created by sissoko on 03/03/2017.
 */
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RoutingModule
    ],
    exports: [
        BudgetComponent
    ],
    declarations: [
        BudgetComponent,
        TransactionComponent,
        NewTransactionComponent,
        TransactionPaginationComponent,
        AmountsComponent,
        NewBudgetComponent,
        BudgetList,
        TransactionList
    ],
    providers: [
        BudgetService,
        BudgetActions,
        UtilService
    ]
})
export class BudgetModule {
}