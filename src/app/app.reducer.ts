import {AppState, TransactionState, LoginState, BudgetState} from "./app.state";
import {combineReducers} from "redux";
import {AppAction} from "./common/app.action";
import {TRANSACTION_ACTIONS, BUDGET_ACTIONS} from "./app.action";
import {LOGIN_ACTIONS} from "./login/login.action";

export function loginReducer(state: LoginState = <LoginState>{connected: {}, auth: false}, action: AppAction) {
    switch (action.type) {
        case LOGIN_ACTIONS.SET_CONNECTED:
            return Object.assign({}, state, action.payload);
        case LOGIN_ACTIONS.SET_AUTH:
            return Object.assign({}, state, {auth: action.payload});
        default:
            return state;
    }
}

export function transactionReducer(state: TransactionState = {}, action: AppAction) {
    let transaction;
    let entities;
    switch (action.type) {
        case TRANSACTION_ACTIONS.SET_VALUE:
            return Object.assign({}, state, {value: action.payload});
        case TRANSACTION_ACTIONS.SET_VALUES:
            return Object.assign({}, state, {values: action.payload});
        case TRANSACTION_ACTIONS.SET_BUDGET:
            return Object.assign({}, state, {budgetId: action.payload});
        case TRANSACTION_ACTIONS.CREATE:
            transaction = action.payload;
            entities = [transaction, ...state.values.entities];
            return Object.assign({}, state, {values: Object.assign({}, state.values, {entities: entities})});
        case TRANSACTION_ACTIONS.DELETE:
            transaction = action.payload;
            entities = state.values.entities.filter(val => val.id != transaction.id);
            return Object.assign({}, state, {
                values: Object.assign({}, state.values, {entities: entities})
            });
        default:
            return state;
    }
}

export function budgetReducer(state: BudgetState = {}, action: AppAction) {
    let transaction;
    let budget;
    let spent;
    let credit;
    let total;
    let entities;
    switch (action.type) {
        case BUDGET_ACTIONS.SET_VALUE:
            return Object.assign({}, state, {value: action.payload});
        case BUDGET_ACTIONS.SET_VALUES:
            return Object.assign({}, state, {values: action.payload});
        case BUDGET_ACTIONS.SET_AMOUNTS:
            return Object.assign({}, state, {amounts: action.payload});
        case BUDGET_ACTIONS.CREATE:
            budget = action.payload;
            entities = [budget, ...state.values.entities];
            return Object.assign({}, state, {values: Object.assign({}, state.values, {entities: entities})});
        case BUDGET_ACTIONS.DELETE:
            budget = action.payload;
            entities = state.values.entities.filter(val => val.id != budget.id);
            return Object.assign({}, state, {
                values: Object.assign({}, state.values, {entities: entities})
            });
        case TRANSACTION_ACTIONS.CREATE:
            transaction = action.payload;
            spent = state.amounts.spent || 0;
            credit = state.amounts.credit || 0;
            total = (state.amounts.total || 0) + transaction.amount;
            if (transaction.amount < 0) {
                spent += transaction.amount;
            } else {
                credit += transaction.amount;
            }
            return Object.assign({}, state, {
                amounts: {
                    spent: spent,
                    credit: credit,
                    total: total
                }
            });
        case TRANSACTION_ACTIONS.DELETE:
            transaction = action.payload;
            spent = state.amounts.spent || 0;
            credit = state.amounts.credit || 0;
            total = (state.amounts.total || 0) - transaction.amount;
            if (transaction.amount < 0) {
                spent -= transaction.amount;
            } else {
                credit -= transaction.amount;
            }
            return Object.assign({}, state, {
                amounts: {spent: spent, credit: credit, total: total}
            });
        default:
            return state;
    }
}

export const rootReducer = combineReducers<AppState>({
    login: loginReducer,
    transactions: transactionReducer,
    budgets: budgetReducer
});