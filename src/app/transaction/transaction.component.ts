import {Component, Input, OnInit} from "@angular/core";
import {NgRedux, select} from "@angular-redux/store";
import {Transaction} from "../app.model";
import {Observable} from "rxjs";
import {AppState} from "../app.state";
import {Pagination} from "../common/app.model";
import {ActivatedRoute} from "@angular/router";
import {TransactionActions, TRANSACTION_ACTIONS, BudgetActions, BUDGET_ACTIONS} from "../app.action";
import {UtilService} from "../common/app.utils";
import {PaginationComponent} from "../common/app.pagination";
import {BaseList, BaseNewComponent} from "../common/common.component";

@Component({
    selector: "transactions",
    template: `
        <amounts></amounts>
        <div class="transactions">
            <new-transaction *ngIf="addNew" [budgetId]="budgetId" [details]="false" (onHide)="addNew=false"></new-transaction>
            <transaction-list [values]="values" (onRemove)="remove($event)"></transaction-list>
            <transaction-pagination [prevText]="'Newer'" [nextText]="'Older'" [budgetId]="budgetId">
                <li class="center-block">
                    <button [disabled]="addNew" class="add btn btn-default" (click)="addTransaction();"><i class="glyphicon glyphicon-plus"></i></button>
                </li>
            </transaction-pagination>
        </div>
    `
})
export class TransactionComponent {
    @select((state: AppState) => state.transactions.values) values$: Observable<Pagination<Transaction>>;
    values: Transaction[] = [];
    page: number = 1;
    pageSize: number = 6;
    addNew: boolean = false;
    budgetId: number;

    constructor(private ngRedux: NgRedux<AppState>,
                private actions: TransactionActions,
                private budgetActions: BudgetActions,
                private activatedRouter: ActivatedRoute,
                private utils: UtilService) {
        this.values$.subscribe(values => {
            if (values) {
                this.values = values.entities;
            }
        });
        activatedRouter.params
            .map(params => params)
            .subscribe(values => {
                this.budgetId = values['budgetId'];
                this.loadTransactions();
                this.loadAmounts();
            })
    }

    private loadTransactions() {
        let action = this.budgetActions.getTransactions(this.budgetId, this.page, this.pageSize);
        action.payload.then(page => {
                this.ngRedux.dispatch({
                    type: TRANSACTION_ACTIONS.SET_VALUES,
                    payload: page
                });
            },
            this.handleError.bind(this));
    }

    private loadAmounts() {
        let action = this.budgetActions.getAmounts(this.budgetId);
        action.payload.then(page => {
                this.ngRedux.dispatch({
                    type: BUDGET_ACTIONS.SET_AMOUNTS,
                    payload: page
                });
            },
            this.handleError.bind(this));
    }

    remove(transaction: Transaction) {
        let action = this.actions.remove(transaction);
        action.payload.then(res => {
            this.ngRedux.dispatch({
                type: TRANSACTION_ACTIONS.DELETE,
                payload: transaction
            });
        }).catch(this.handleError.bind(this));
    }

    inOrOut(val: number): string {
        return this.utils.inOrOut(val);
    }

    private handleError(error: any) {
        this.utils.handleError(error);
    }

    addTransaction() {
        console.log("addTransaction()");
        this.addNew = true;
    }

}

@Component({
    selector: 'transaction-list',
    template: `
        <div class="list-group">
            <span *ngFor="let transaction of values"
               class="list-group-item transaction-{{inOrOut(transaction.amount)}} transaction" (click)="$event.stopPropagation()">
                <span class="badge {{inOrOut(transaction.amount)}} balance transaction-{{inOrOut(transaction.amount)}}-badge">{{transaction.amount | currency:'EUR':true:'1.2-2'}}</span>
                <h4 class="list-group-item-heading text-uppercase">{{transaction.date | date:'dd MMM.'}}</h4>
                <p class="list-group-item-text description text-uppercase">
                    {{transaction.description ? transaction.description : 'Description'}}
                    <span class="badge btn-danger delete pull-right" (click)="remove(transaction)">X</span>
                </p>
    
            </span>
        </div>
    `
})
export class TransactionList extends BaseList<Transaction> {

    constructor(private utils: UtilService) {
        super();
    }

    inOrOut(val: number): string {
        return this.utils.inOrOut(val);
    }
}

@Component({
    selector: 'new-transaction',
    template: `
        <form role="form">
            <fieldset>
                <input type="hidden" [(ngModel)]="value.budgetId" name="budgetId">
                <div class="form-group">
                    <input placeholder="Amount" class="form-control" type="text" [(ngModel)]="value.amount" name="amount"/>
                </div>
                <div class="form-group">
                    <input placeholder="description" class="form-control" type="text" [(ngModel)]="value.description"
                           name="description"/>
                </div>
                <div *ngIf="message">
                    <span class="alert-danger">{{message}}</span>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <button type="submit" (click)="add()" class="btn btn-lg btn-success btn-block">Add</button>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <button (click)="hide($event)" class="btn btn-lg btn-default btn-block">Hide</button>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr class="colorgraph">
        <div *ngIf="details" class="transactions new">
            <transaction-list [values]="values" (onRemove)="remove($event)"></transaction-list>
        </div>
    `
})
export class NewTransactionComponent extends BaseNewComponent<Transaction> implements OnInit {
    @Input() budgetId;
    constructor(ngRedux: NgRedux<AppState>,
                actions: TransactionActions,
                utils: UtilService) {
        super(ngRedux, actions, utils);
        this.value = <Transaction>{};
    }

    ngOnInit(): void {
        this.value.budgetId = this.budgetId;
    }
}

@Component({
    selector: 'transaction-pagination',
    templateUrl: '../common/app.pagination.html'
})
export class TransactionPaginationComponent extends PaginationComponent<Transaction> {
    @select((state: AppState) => state.transactions.values) values$: Observable<Pagination<Transaction>>;
    @Input() budgetId: number;

    actions: TransactionActions;
    /**
     *
     * @param ngRedux
     * @param actions
     */
    constructor(ngRedux: NgRedux<AppState>, actions: TransactionActions) {
        super(ngRedux, actions);
        this.actions = actions;
        this.values$.subscribe(values => {
            if (values) {
                this.page = values.page;
                this.pageSize = values.pageSize;
                this.pageCount = values.pageCount;
                this.totalEntityCount = values.totalEntityCount;
            }
        });
    }

    protected load(page: number, pageSize: number) {
        let action = this.actions.getTransactions(this.budgetId, page, pageSize);
        action.payload.then(page => {
            this.ngRedux.dispatch({
                type: this.actions.getAction('SET_VALUES'),
                payload: page
            });
        });
    }
}