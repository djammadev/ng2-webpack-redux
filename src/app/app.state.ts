import {Transaction, Amounts, User, Budget} from "./app.model";
import {IAppState, BaseState} from "./common/app.state";

export interface AppState extends IAppState {
    login?: LoginState;
    transactions?: TransactionState;
    budgets?: BudgetState;
}

export class LoginState {
    auth?: boolean;
    connected?: User;
}

export interface TransactionState extends BaseState<Transaction> {
}

export interface BudgetState extends BaseState<Budget> {
    amounts?: Amounts;
}
