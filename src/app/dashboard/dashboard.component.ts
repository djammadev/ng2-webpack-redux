import {Component} from "@angular/core";
import {NgRedux} from "@angular-redux/store";
import {AppState} from "../app.state";

@Component({
    selector: 'dashboard',
    template: `
        <budget></budget>
    `
})
export class DashboardComponent {

    /**`
     *
     * @param ngRedux
     */
    constructor(private ngRedux: NgRedux<AppState>) {
    }
}
