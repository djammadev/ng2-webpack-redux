import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {AppState} from "../app.state";
import {User} from "../app.model";
import {LoginActions, LOGIN_ACTIONS} from "../login/login.action";
import {NgRedux} from "@angular-redux/store";

@Component({
    selector: "app-register",
    templateUrl: "./register.component.html"
})
export class RegisterComponent {
    user: User;
    message: string;

    constructor(private ngRedux: NgRedux<AppState>, private actions: LoginActions, private router: Router) {
        this.user = <User>{};
    }

    register() {
        let action = this.actions.register(this.user);
        action.payload.then(user => {
            this.ngRedux.dispatch({
                type: LOGIN_ACTIONS.SET_CONNECTED,
                payload: {connected: user, auth: true}
            });
            this.router.navigate(['/'])
        }, error => {
            this.ngRedux.dispatch({
                type: LOGIN_ACTIONS.SET_CONNECTED,
                payload: {connected: {}, auth: false}
            });
            let messageJson = error.json();
            this.message = messageJson.message;
        });
    }
}
