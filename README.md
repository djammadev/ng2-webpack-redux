# Angular 2 Webpack Redux

## Install
`git checkout https://csissoko@bitbucket.org/djammadev/ng2-webpack-redux.git`

`cd ng2-webpack-redux`

`npm install`

## Run

`npm start`

[http://localhost:8090](http://localhost:8090)

## Demo

[https://candm.herokuapp.com](https://candm.herokuapp.com)