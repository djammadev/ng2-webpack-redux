/**
 * Created by sissoko on 23/02/2017.
 */
var webpackMerge = require('webpack-merge');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

const ENV = process.env.ENV = process.env.NODE_ENV = 'development';

module.exports = function (options) {
    return webpackMerge(commonConfig({env: ENV}), {
        devtool: 'cheap-module-eval-source-map',
        devServer: {
            historyApiFallback: true,
            stats: 'minimal',
            proxy: {
                '/api/*': {
                    target: 'http://localhost:8080',
                    secure: false,
                    bypass: function (req, res, proxyOptions) {
                        var accept = req.headers.accept;
                        if (accept && accept.indexOf('html') !== -1) {
                            console.log('Skipping proxy for browser request.');
                            return '/index.html';
                        }
                    }
                }
            }
        }
    });
}

