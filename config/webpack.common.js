/**
 * Created by sissoko on 23/02/2017.
 */
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var helpers = require('./helpers');

module.exports = function (options) {
    isProd = options.env === 'production';
    return {
        entry: {
            app: './src/main.ts',
            vendor: './src/vendor.ts'
        },
        output: {
            path: helpers.root('dist'),
            filename: '[name].bundle.js'
        },
        module: {
            loaders: [
                {
                    test: /\.ts$/,
                    loader: 'ts-loader!angular2-template-loader'
                },
                {
                    test: /\.html$/,
                    loader: 'html-loader'
                },
                /*
                 * to string and css loader support for *.css files (from Angular components)
                 * Returns file content as string
                 *
                 */
                {
                    test: /\.css$/,
                    loader: 'to-string-loader!css-loader',
                    exclude: [helpers.root('src', 'styles')]
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    loaders: [
                        'file-loader?name=assets/[name].[ext]',
                        'image-webpack-loader?progressive=true'
                    ]
                }
            ]
        },
        resolve: {
            extensions: ['.js', '.ts']
        },
        plugins: [
            new webpack.ContextReplacementPlugin(
                /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
                helpers.root('src')
            ),
            new HtmlWebpackPlugin({
                template: './src/index.html'
            }),
            new CopyWebpackPlugin([
                {
                    from: helpers.root('src', 'assets'),
                    to: 'assets'
                },
                {
                    from: helpers.root('src', 'styles'),
                    to: 'styles'
                }
            ])
        ]
    };
}