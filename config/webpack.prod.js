/**
 * Created by sissoko on 23/02/2017.
 */

var webpackMerge = require('webpack-merge');
var commonConfig = require('./webpack.common.js');

const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

module.exports = function (options) {
    return webpackMerge(commonConfig({env: ENV}));
}